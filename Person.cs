namespace consoleApp
{
    public class Person 
    {
        public Person() {}
        // I think there is a shorter way of doing this (will look into later)
        // too used to automapper 
        public Person
            (
                int Id, 
                string firstname, 
                string lastname,
                string address,
                string city
            )
        {
            PersonID = Id;
            FirstName = firstname;
            LastName = lastname;
            Address = address;
            City = city;
        }

        public int PersonID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
    }
}
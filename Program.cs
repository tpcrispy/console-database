﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using consoleApp;

namespace consoletoapi {
  class Program {
    static async Task Main(string[] args) {
      // DB CONNECT STRING 
      string ConString = @"Server=127.0.0.1,1433;User Id=SA;Database=testdatabase;Password=reallyStrongPwd123";
      List < Person > PersonData = new List < Person > ();

      using(SqlConnection connection = new SqlConnection(ConString)) {
        connection.Open();
        string query = "select * from Persons";
        SqlCommand cm = new SqlCommand(query, connection);
        SqlDataReader sdr = cm.ExecuteReader();

        // CAST THE DATA TO A NEW PERSON OBJECT && ADD TO PersonData LIST 
        while (sdr.Read()) {
          Person PersonRow = new Person(
            (int) sdr["PersonID"],
            (string) sdr["FirstName"],
            (string) sdr["LastName"],
            (string) sdr["Address"],
            (string) sdr["City"]
          );
          PersonData.Add(PersonRow);
        }
      }
      // SEND THE PERSONDATA LIST TO API
      using(HttpClient client = new HttpClient()) {
        var result = await client.PostAsJsonAsync("https://localhost:5001/api/db/upload", PersonData);
        Console.WriteLine(result);
        // ^^ ADD ERROR HANDLING 
      }
    }
  }
}